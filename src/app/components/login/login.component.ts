import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  campos = [
    {label:'Usuario',type:"email",id:"user"},
    {label:'Contraseña',type:"password",id:"clave"},
    {label:'',type:"button", class:"btn btn-primary",value:"Ingresar"}
  ]


  observableSubscription!: Subscription;
  constructor(
    private userService: UserService,
    private router: Router,
    private toast: ToastrService
  ) { }

  ngOnInit(): void {
  }

  handle(event:any)
  {
    this.userService.login(event)
    .subscribe((res:any) => {
      if(res){
        localStorage.setItem('user',res);
        this.router.navigate(['/empleados']);
      }else{
        this.toast.error('Usuario Inválido');
      }
    });
  }

}
