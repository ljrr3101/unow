import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { EmpleadosService } from '../../services/empleados.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html',
  styleUrls: ['./empleados.component.css']
})
export class EmpleadosComponent implements OnInit {

  empleados:any[] = [];
  constructor(
    private router: Router,
    private empleadoServices: EmpleadosService,
    private userService: UserService
  ) { }

  ngOnInit(): void {

    this.userService.isSessionActive()
    .subscribe((res:any)=>{
      if(!res)
      {
        this.router.navigate(['/'])
      }
      else{
        this.getEmpleados();
      }
    })

  }

  getEmpleados()
  {
    this.empleadoServices.getAll()
    .subscribe((res:any)=>{
      this.empleados = res;
    });
  }



}
