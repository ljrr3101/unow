import { Component, Input, OnInit, Output,EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidationErrors, EmailValidator  } from '@angular/forms';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  @Input() campos:any[] = []
  @Input() class:any;
  @Input() logo:any;

  isValid: boolean = false;
  form: FormGroup;


  @Output() data: EventEmitter<any> = new EventEmitter();
  constructor(
    private fb: FormBuilder
  ) {
    this.form = this.fb.group({
      usuario:['',[Validators.required,Validators.email]],
      clave:['',[Validators.required,Validators.minLength(8)]]
    });
   }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm()
  {
    this.form = this.fb.group({
      usuario:['',[Validators.required,Validators.email]],
      clave:['',[Validators.required,Validators.minLength(8)]]
    });
  }

  checkForm(event?:string){
    if(this.form.status !== "INVALID") this.isValid = true
  }

  send()
  {
    let data:any = this.form.value;

    this.data.emit(data);
  }



}
