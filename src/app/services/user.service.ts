import { Injectable } from '@angular/core';
import { Observable, Observer, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private users = [
    {id:1,usuario:'admin@endalia.com',clave:'3ndalia@2022'},
    {id:2,usuario:'develop@endalia.com',clave:'3ndalia@2022'},
    {id:3,usuario:'manager@endalia.com',clave:'3ndalia@2022'},
  ]




  constructor() { }

  login(data:any):Observable<any>
  {
    let user = this.users.find( u => u.usuario === data.usuario && u.clave === data.clave);
    return new Observable((observer: Observer<any>) => {
      observer.next(user);
      observer.complete();
    });
  }

  logout(data?:any):Observable<any>
  {
    localStorage.removeItem('user');
    return new Observable((observer: Observer<any>) => {
      observer.next(true);
      observer.complete();
    });
  }

  isSessionActive(data?:any):Observable<any>
  {
    let user = localStorage.getItem('user');
    const active = (user !== null ) ? true : false;
    return new Observable((observer: Observer<any>) => {
      observer.next(active);
      observer.complete();
    });
  }
}
