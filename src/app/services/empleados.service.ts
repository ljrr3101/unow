import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpleadosService {

  private empleados = [
    {id:1,apellidos:'Abad Jimenez',nombre:'Ingacio',genero:'h',puesto:'Representante RRHH',telefono:'8174139',correo:'ignacio.abad @endalia.com',},
    {id:2,apellidos:'Aguirre Leon',nombre:'Verónica',genero:'m',puesto:'Operario Servicio',telefono:'1783984',correo:'veronica.aguirre@endalia.com',},
    {id:3,apellidos:'Aguirre Rivera',nombre:'Miguel Angel',genero:'h',puesto:'Direccion Comercial',telefono:'3871589',correo:'miguel.aguirre@endalia.com',},
    {id:4,apellidos:'Alcalá',nombre:'Carlos',genero:'h',puesto:'Auditor Medio Ambiente',telefono:'4138745',correo:'carlos.alcala@endalia.com',},
    {id:5,apellidos:'Alcalá Ordoñez',nombre:'Angela',genero:'m',puesto:'Asistente Admnistrativo',telefono:'8547632',correo:'angela.alcala@endalia.com',},
    {id:6,apellidos:'Alvarez Pineda',nombre:'Laura',genero:'m',puesto:'Representante RRHH',telefono:'8174139',correo:'laura.alvarez@endalia.com'},
  ]

  constructor() { }

  getAll(){
    return new Observable((observer: Observer<any>) => {
      observer.next(this.empleados);
      observer.complete();
    });
  }

  search(search:string):Observable<any>
  {
    let resultado = this.empleados.filter( e => e.apellidos === search || e.nombre === search || e.puesto === search || e.telefono === search || e.correo === search);
    return new Observable((observer: Observer<any>) => {
      observer.next(resultado);
      observer.complete();
    });
  }
}
